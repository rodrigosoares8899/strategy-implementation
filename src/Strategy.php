<?php


namespace Pattern;


interface Strategy
{
    public function applyTransferRules(array $accountData): void;
}