<?php


namespace Pattern;


class Context
{

    private $strategy;

    public function __construct(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    public function setStrategy(Strategy $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function transferMoney($data): void
    {

        echo "The context will transfer your money... \n";

        $this->strategy->applyTransferRules($data);

    }

}