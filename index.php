<?php

include_once "bootstrap.php";

$dataTransfer = array(
    'name' => 'Rodrigo',
    'value' => 50.000,
    'currency' => 'BRL',
    'destination' => 'Canada'
);

$transfer = null;

switch ($dataTransfer["destination"]) {
    case 'USA':
        $transfer = new Pattern\TransferToUSA();
        break;
    case 'UK':
        $transfer = new \Pattern\TransferToUK();
        break;
    case 'Canada':
        $transfer = new \Pattern\TransferToCanada();
}

$context = new \Pattern\Context($transfer);

$context->transferMoney($dataTransfer);

?>